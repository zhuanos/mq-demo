package cn.itcast.mq.listener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
/**
 * @Auth: zhuan
 * @Desc: MQ消息接收者
 * @Date: 2023/8/22 12:11
 */
@Component
public class MqListener {

    @RabbitListener(queues = {"simple.queue"})
    public void receiveMsg(String msg){
        System.out.println("接收到消息："+msg);
    }
    @RabbitListener(queues = {"fanout.queue1"})
    public void receiveFanoutMsg(String msg){
        System.out.println("接收到Fanout消息："+msg);
    }
    @RabbitListener(queues = {"fanout.queue2"})
    public void receiveFanoutMsg2(String msg){
        System.out.println("接收到Fanout消息："+msg);
    }



}
