package cn.itcast.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DirectConfig {

    @Bean
    public DirectExchange directExchange(){
        DirectExchange exchange = ExchangeBuilder.directExchange("direct.exchange").build();
        return exchange;
    }
    @Bean
    public Queue directQueue1(){
        Queue queue = QueueBuilder.durable("direct.queue1").build();
        return queue;
    }
    @Bean
    public Queue directQueue2(){
        Queue queue = QueueBuilder.durable("direct.queue2").build();
        return queue;
    }
    @Bean
    public Binding directBinding1(){
        Binding binding = BindingBuilder.bind(directQueue1()).to(directExchange()).with("red");
        return binding;
    }
    @Bean
    public Binding directBinding2(){
        Binding binding = BindingBuilder.bind(directQueue1()).to(directExchange()).with("blue");
        return binding;
    }
    @Bean
    public Binding directBinding3(){
        Binding binding = BindingBuilder.bind(directQueue2()).to(directExchange()).with("red");
        return binding;
    }
    @Bean
    public Binding directBinding4(){
        Binding binding = BindingBuilder.bind(directQueue2()).to(directExchange()).with("yellow");
        return binding;
    }
}
