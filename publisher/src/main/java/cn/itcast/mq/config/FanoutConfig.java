package cn.itcast.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * @Auth: zhuan
 * @Desc: MQ配置类
 * @Date: 2023/8/22 16:49
 */
@Configuration
public class FanoutConfig {

    @Bean
    public FanoutExchange fanoutExchange(){
        FanoutExchange exchange = ExchangeBuilder.fanoutExchange("fanout.exchange").build();
        return exchange;
    }
    @Bean
    public Queue queue1(){
        Queue queue = QueueBuilder.durable("fanout.queue1").build();
        return queue;
    }
    @Bean
    public Queue queue2(){
        Queue queue = QueueBuilder.durable("fanout.queue2").build();
        return queue;
    }

    @Bean
    public Binding binding1(){
        Binding binding = BindingBuilder.bind(queue1()).to(fanoutExchange());
        return binding;
    }
    @Bean
    public Binding binding2(){
        Binding binding = BindingBuilder.bind(queue2()).to(fanoutExchange());
        return binding;
    }
}
