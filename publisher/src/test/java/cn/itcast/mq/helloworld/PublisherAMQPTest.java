package cn.itcast.mq.helloworld;

import cn.itcast.mq.PublisherApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = PublisherApplication.class)
@RunWith(SpringRunner.class)
public class PublisherAMQPTest {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Test
    public void testSimpleQueue() {
        // 1.队列名称
        String queueName = "simple.queue";
        // 2.消息
        String message = "hello, spring amqp!";
        // 3.使用模板对象完成消息发送
        rabbitTemplate.convertAndSend(queueName,message);
    }
    @Test
    public void testFanout() {
        // 1.交换机名称 fanout.exchange
        String exchangeName = "fanout.exchange";
        // 2.消息
        String message = "hello, spring publisher/subscribe!";
        // 3.使用模板对象完成消息发送
        rabbitTemplate.convertAndSend(exchangeName,"",message);
    }
    @Test
    public void testDirect() {
        // 1.交换机名称 fanout.exchange
        String exchangeName = "direct.exchange";
        // 2.消息
        String message = "hello, spring routing!";
        // 3.使用模板对象完成消息发送
        rabbitTemplate.convertAndSend(exchangeName,"blue",message);
    }

}
